import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { VoteUpAction, VoteDownAction } from '../models/destinos-viajes-state.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  // tslint:disable-next-line: no-input-rename
  @Input('idx') position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<DestinoViaje>;


  constructor(
    private store: Store<AppState>
  ) {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  // tslint:disable-next-line: typedef
  ir() {
    this.clicked.emit(this.destino);
    return false;
  }

  // tslint:disable-next-line: typedef
  voteUp() {
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  // tslint:disable-next-line: typedef
  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }

}
