import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { DestinosApiClient } from '../models/destinos-api-cliente.model';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAccion, DestinosViajesState } from '../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;

  constructor(
    // tslint:disable-next-line: no-shadowed-variable
    public destinosApiClient: DestinosApiClient,
    private store: Store<AppState>
  ) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
      .subscribe(data => {
        const d = data;
        if (d != null) {
          this.updates.push('Esta elegido: ' + d.nombre);
        }
      });

    store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit(): void {
    this.store.select(state => state.destinos)
      .subscribe(data => {
        const d = data.favorito;
        if (d != null) {
          this.updates.push('Esta elegido: ' + d.nombre);
        }
      });
  }

  // tslint:disable-next-line: typedef
  agregado(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  // tslint:disable-next-line: typedef
  elegido(e: DestinoViaje) {
    this.destinosApiClient.elegir(e);
  }

  // tslint:disable-next-line: typedef
  getAll(){

  }

}
