import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';

/*redux*/
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { DestinosViajesState, NuevoDestinoAccion, ElegidoFavoritoAction } from './destinos-viajes-state.model';
import { AppState } from '../app.module';

@Injectable()
export class DestinosApiClient{

    destinos: DestinoViaje[] = [];

    constructor(
        private store: Store<AppState>
    ){ }
    // tslint:disable-next-line: typedef
    add(d: DestinoViaje){
        this.store.dispatch(new NuevoDestinoAccion(d));
    }
    getAll(): DestinoViaje[]{
        return this.destinos;
    }
    getById(id: string): DestinoViaje{
        // tslint:disable-next-line: typedef
        return this.destinos.filter(function(d) {return d.id.toString() === id; })[0];
    }

    // tslint:disable-next-line: typedef
    elegir(d: DestinoViaje) {
        this.store.dispatch(new ElegidoFavoritoAction(d));
    }
}
