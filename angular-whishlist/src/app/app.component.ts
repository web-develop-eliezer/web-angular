import { Component } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'angular-whishlist';
  time = new Observable(Observer => {
    setInterval(() => Observer.next(new Date().toString()), 1000)
  });
  // tslint:disable-next-line: typedef
  destinoAgregado(d) {
    
  }
}
